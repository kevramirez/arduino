#include<LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
typedef enum {
  HORLOGE_IDLE,
  HORLOGE_MIN,
  HORLOGE_HEURE
} HORLOGE_STATE;

HORLOGE_STATE hstate;

int timer, cpt_min, cpt_heure = 0;
int chrono = 1000;


void HORLOGE_init()
{
  hstate = HORLOGE_IDLE;
}

void HORLOGE_update()
{
  HORLOGE_STATE nextState = hstate;

  switch (hstate) {
    case HORLOGE_IDLE:
      timer = millis();
      Serial.println("IDLE");
      delay(100);
      nextState = HORLOGE_MIN;    
      break;

    case HORLOGE_MIN:
      Serial.println("MINUTE");
      cpt_min++;
      if (cpt_min != 60) {
        chrono = millis()+1000;
        Serial.println(cpt_min);
        delay(100);
        nextState = HORLOGE_IDLE;
      }
      if (cpt_min == 60) {
        cpt_min = 0;
        cpt_heure++;
        Serial.println(cpt_heure + " h " + cpt_min);
        delay(100);
        nextState = HORLOGE_IDLE;
       // nextState = HORLOGE_HEURE;
      }
      break;
      
    case HORLOGE_HEURE:
      cpt_heure++;
      if (cpt_heure != 24) {
        nextState = HORLOGE_IDLE;
      }
      if (cpt_heure == 24) {
        cpt_heure = 0;
        nextState = HORLOGE_IDLE;
      }
      break;
      
  }hstate = nextState;
}
void setup() {
  
  Serial.begin(9600);
  HORLOGE_init();
}

void loop() {
  HORLOGE_update();
  
  lcd.clear();
 
  lcd.setCursor(1,0);
  lcd.print(cpt_heure);
  lcd.print(":");
  lcd.print(cpt_min);

  
}
